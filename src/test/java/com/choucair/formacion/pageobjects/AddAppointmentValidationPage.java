package com.choucair.formacion.pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class AddAppointmentValidationPage extends PageObject{
		
		//Menu Add Appointment
				@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[6]")
				public WebElement MenuAddAppointment;
				
			//Field Date
				@FindBy(xpath="//*[@id=\'datepicker\']")
				public WebElementFacade fromDateBox;
				
			//Field Identification Patient
				@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
				public WebElementFacade txtIdentificationPatient;
				
			//Field Identification Doctor
				@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
				public WebElementFacade txtIdentificationDoctor;
				
			//observations
				@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/textarea")
				public WebElementFacade txtObservations;
				
			//Field Button save
				@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/a")
				public WebElementFacade btnSave;
						
				
				public void Menuvalidation() {
					MenuAddAppointment.click();
				}
				
				public void Date(String dataUser) {
					fromDateBox.sendKeys(dataUser);
					fromDateBox.sendKeys(Keys.ENTER);
				}
								
				public void IdPatient(String dataUser) {
					txtIdentificationPatient.click();
					txtIdentificationPatient.clear();
					txtIdentificationPatient.sendKeys(dataUser);				
				}
				
				public void IdDoctor(String dataUser) {
					txtIdentificationDoctor.click();
					txtIdentificationDoctor.clear();
					txtIdentificationDoctor.sendKeys(dataUser);				
				}
				
				public void Observations(String dataUser) {
					txtObservations.click();
					txtObservations.clear();
					txtObservations.sendKeys(dataUser);	
					
				}
				
							
				public void Buttonsave() {
					btnSave.click();
				}
				

	}

