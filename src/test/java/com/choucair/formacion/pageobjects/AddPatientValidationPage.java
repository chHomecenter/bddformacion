package com.choucair.formacion.pageobjects;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class AddPatientValidationPage extends PageObject {
	
	//Menu Add patient
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[2]/div/div/div/div/div[1]/div/a[2]")
			public WebElement MenuAddPatient;
			
		//Field Name
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[1]/input")
			public WebElementFacade txtName;
			
		//Field Last Name
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input")
			public WebElementFacade txtLastName;
			
		//Field Telephone
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input")
			public WebElementFacade txtTelephone;
			
		//Field Identification Type
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/select")
			public WebElementFacade cbmTypeIdentification;
			
		//Field Identification
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/div[5]/input")
			public WebElementFacade txtIdentification;
		
		//Field Health
			@FindBy(xpath="//*[@id=\"page-wrapper\"]/div/div[3]/div/div[6]/label")
			public WebElementFacade chkHealth;
						
	   //Field Button save
			@FindBy(xpath="//*[@id=\'page-wrapper\']/div/div[3]/div/a")
			public WebElementFacade btnSave;
					
			
			public void Menuvalidation() {
				MenuAddPatient.click();
			}
			
			public void Name(String dataUser) {
				txtName.click();
				txtName.clear();
				txtName.sendKeys(dataUser);
							
			}
				
			public void Lastname(String dataUser) {
				txtLastName.click();
				txtLastName.clear();
				txtLastName.sendKeys(dataUser);				
			}
			
			public void Telephone(String dataUser) {
				txtTelephone.click();
				txtTelephone.clear();
				txtTelephone.sendKeys(dataUser);		
				
			}
			
			public void Identificationtype(String dataUser) {
				cbmTypeIdentification.click();
				cbmTypeIdentification.selectByVisibleText(dataUser);
				
			}
			
			public void Identification(String dataUser) {
				txtIdentification.click();
				txtIdentification.clear();
				txtIdentification.sendKeys(dataUser);				
			}
			
			public void Checkhealt() {
				chkHealth.click();
			}
			
			public void Buttonsave() {
				btnSave.click();
			}
			

}
