package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.AddAppointmentValidationSteps;
import com.choucair.formacion.steps.AddDoctorValidationSteps;
import com.choucair.formacion.steps.AddPatientValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class MedicalAppointmentValidationDefinition {
	
	@Steps
	AddDoctorValidationSteps addDoctorValidationSteps;

	@Steps
	AddPatientValidationSteps addPatientValidationSteps;
	
	@Steps
	AddAppointmentValidationSteps addAppointmentValidationSteps;
	
	
	@Given("^Add a doctor$")
	public void add_a_doctor() {
		addDoctorValidationSteps.menu_validation_doctor();
	   
	}
	
	@Given("^Add a patient$")
	public void add_a_patient()  {
		addPatientValidationSteps.menu_validation_patient();
	    
	}
	
	@Given("^Add a appointment$")
	public void add_a_appointment()  {
		addAppointmentValidationSteps.menu_appointment();
	    
	}

	@When("^Add a doctor in medical appointments$")
	public void add_a_doctor_in_medical_appointments(DataTable dtDatosForm) throws InterruptedException{
		List<List<String>> data = dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			addDoctorValidationSteps.fill_form_doctor_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	@When("^Add a patient in medical appointments$")
	public void add_a_patient_in_medical_appointments(DataTable dtDatosForm) throws InterruptedException{
		List<List<String>> data = dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			addPatientValidationSteps.fill_form_patient_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	@When("^Add a appointment in medical appointments$")
	public void add_a_appointment_in_medical_appointments(DataTable dtDatosForm) throws InterruptedException{
		List<List<String>> data = dtDatosForm.raw();
		Thread.sleep(5000);
		for (int i=1; i<data.size(); i++) {
			
			addAppointmentValidationSteps.fill_form_appointment_data_table(data, i);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
			}
		}
	}


	@Then("^successful form$")
	public void successful_form() {
	   
	}
	
	@Then("^successful form Patient$")
	public void successful_form_Patient() {
	 
	}
	
	@Then("^successful form Appointment$")
	public void successful_form_Appointment()  {
	  
	}

}







