package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.AddAppointmentValidationPage;
import net.thucydides.core.annotations.Step;

public class AddAppointmentValidationSteps {
	
	AddAppointmentValidationPage addAppointmentValidationPage;
	
	@Step
	public void fill_form_appointment_data_table(List<List<String>>data, int id) {
		addAppointmentValidationPage.Date(data.get(id).get(0).trim());
		addAppointmentValidationPage.IdPatient(data.get(id).get(1).trim());
		addAppointmentValidationPage.IdDoctor(data.get(id).get(2).trim());
		addAppointmentValidationPage.Observations(data.get(id).get(3).trim());
		addAppointmentValidationPage.Buttonsave();
		
	}
	
	@Step
	public void menu_appointment() {
		addAppointmentValidationPage.open();
		addAppointmentValidationPage.Menuvalidation();
	}
	
}

