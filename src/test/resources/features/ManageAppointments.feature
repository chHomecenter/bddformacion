
@tag
Feature: Management of medical appointments
  
  @AddDoctor
  Scenario: Add a doctor medical
    Given Add a doctor
    When  Add a doctor in medical appointments
    |name |lastname   |telephone  |identifictiontype   |Identification|
    |Luisa|De la Pava	|3214567890 |Cédula de ciudadanía|1023868618    |
    Then  successful form Doctor
    

  @AddPatient
  Scenario: Add a patient
    Given Add a patient
    When Add a patient in medical appointments
    |name |lastname   |telephone  |identifictiontype   |Identification|
    |Luisa|Rojas    	|3214567878 |Cédula de ciudadanía|1023868620    |
    Then successful form Patient

   @AddAppointment
  Scenario: Add a appointment
    Given Add a appointment
    When Add a appointment in medical appointments
    |Date      |IdentificactionPatient |IdentificactionDoctor |Observations |
    |03/09/2019|1023868620             |1023868618            |Finalized    |
    Then successful form Appointment
  
 
